/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.cavero.raingame;

import java.util.Comparator;

/**
 *
 *
 */
public class Test {

    public Test() {
        
    }
    
    public Comparator<String> getComparator() {
        
//        return new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//            }
//            
//        };

        return (s1, s2) -> s1.compareTo(s2);
    }
    
    public class StringComparatorInnerClass implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            return o1.compareTo(o2);
        }
    
    }
    
    public static class StringComparatorStaticInnerClass implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
    }
    
}
